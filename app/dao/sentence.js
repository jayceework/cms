import { NotFound } from 'lin-mizar';
import { SentenceModel } from '../models/sentence';

class Sentence {
  static async getSentenceList () {
    const res = await SentenceModel.findAll();
    return res;
  }
  static async addSentence (v) {
    return SentenceModel.create(v);
  }

  static async editSentence (id, params) {
    const sentence = await SentenceModel.findByPk(id);
    if (!sentence) {
      throw new NotFound();
    }
    return sentence.update({ ...params });
  }
  static async deleteSentenceById (id) {
    return SentenceModel.destroy({ where: { id } });
  }
}
export { Sentence as SentenceDao };