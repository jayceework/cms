import { NotFound } from 'lin-mizar';
import { MusicModel } from '../models/music';

class Music {
  static async getMusicList () {
    const res = await MusicModel.findAll();
    return res;
  }
  static async addMusic (v) {
    return MusicModel.create(v);
  }
  static async editMusic (id, params) {
    const music = await MusicModel.findByPk(id);
    if (!music) {
      throw new NotFound();
    }
    return music.update({ ...params });
  }

  static async deleteMusicById (id) {
    return MusicModel.destroy({ where: { id } });
  }
}
export { Music as MusicDao };