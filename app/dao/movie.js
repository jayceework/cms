import { NotFound } from 'lin-mizar';
import { MovieModel } from '../models/movie';

class Movie {
  static async getMovieList () {
    const res = await MovieModel.findAll();
    return res;
  }

  static async addMovie (v) {
    return MovieModel.create(v);
  }

  static async editMovie (id, params) {
    const movie = await MovieModel.findByPk(id);
    if (!movie) {
      throw new NotFound();
    }
    return movie.update({ ...params });
  }

  static async deleteMovieById (id) {
    return MovieModel.destroy({ where: { id } });
  }
}
export { Movie as MovieDao };